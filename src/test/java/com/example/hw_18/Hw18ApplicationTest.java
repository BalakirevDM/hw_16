package com.example.hw_18;

import com.example.hw_18.models.Client;
import com.example.hw_18.models.ClientTemplate;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.annotation.DirtiesContext;
import static org.hamcrest.Matchers.*;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class Hw18ApplicationTest {
    @LocalServerPort
    public Integer port;
    RequestSpecification request;

    @BeforeEach
    public void createRestRequest() {
        request = RestAssured.given();
        RestAssured.baseURI = "http://localhost:" + port;
    }

    @Test
    public void postClientRequestPositive() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("name", "Vasya");
        json.put("amount", 300);
        json.put("age", 35);
        request.contentType("application/json")
        .body(json.toString())
        .post("/credit/add_client")
                .then()
        .statusCode(201);
        Assertions.assertEquals("Vasya", Client.clients.get(Client.clients.size() - 1).getName());
    }

    @Test
    public void deleteClientPositive() throws JSONException {
        Long id;
        if(Client.clients.isEmpty()) {
            ClientTemplate testClient = new ClientTemplate();
            testClient.setName("Vasya");
            testClient.setAge(30);
            testClient.setAmount(20);
            Client client = testClient.toClient();
            Client.clients.add(client);
        }
        id = Client.clients.get(0).getId();
        String idString = id.toString();
        JSONObject deleteRequest = new JSONObject();
        deleteRequest.put("id", idString);
        request.contentType("application/json")
                .body(deleteRequest.toString())
                .delete("/credit/delete_client")
                .then()
                .statusCode(200);
    }

    @Test
    public void postClientRequestNegative() {
        request.contentType("application/json")
                .body("/credit/add_client")
                .then()
                .statusCode(404);
    }

    @Test
    public void getClientPositive() {
        Long id;
        if(Client.clients.isEmpty()) {
            ClientTemplate testClient = new ClientTemplate();
            testClient.setName("Vasya");
            testClient.setAge(30);
            testClient.setAmount(20);
            Client client = testClient.toClient();
            Client.clients.add(client);
        }
        id = Client.clients.get(0).getId();
        request.contentType("application/json")
                .body("/credit/request/" + id)
                .then()
                .statusCode(200);
    }

}