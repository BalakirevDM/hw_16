package com.example.hw_18.endpoints;

import com.example.hw_18.models.Client;
import com.example.hw_18.models.ClientTemplate;
import com.example.hw_18.models.DeleteStatus;
import com.google.gson.Gson;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


//@Tag(name ="CREDIT CLIENT",description = "API for client's requests for credit")
@RequestMapping("/credit")
@RestController
public class CreditRequestApi {

    // private ObjectMapper mapper = new ObjectMapper();

    @GetMapping(path = "/request/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getRequest(@PathVariable Long id) {
        Client clientForGetRequest = Client.clients.stream()
                .filter(n -> n.getId() == id)
                .findFirst().get();
        Gson gson = new Gson();
        return gson.toJson(clientForGetRequest);
    }

    @PostMapping(path = "/add_client", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Long createClientRequest(@RequestBody ClientTemplate temp) {
        // Реализация с генератором случайных персон (ФИО, дата рождения, адрес)
/*        Person person = RandomPerson.get().next();
        Client client = Client.builder().age(person.getAge()).name(person.getFirstName() + " " + person.getLastName()).build();*/

        Client client = temp.toClient();
        Client.clients.add(client);
        return client.getId();
    }

    @DeleteMapping(path = "/delete_client", produces = MediaType.APPLICATION_JSON_VALUE,
    consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String deleteClientRequestStatus(@RequestBody String idRequest) {
        JSONObject request = new JSONObject(idRequest);
        long id = Long.parseLong(request.getString("id"));
        DeleteStatus status = new DeleteStatus(false);
        if(Client.clients.stream().anyMatch(n -> n.getId() == id)) {
            Client.clients.remove(Client.clients.stream()
                    .filter(n -> n.getId() == id)
                    .findFirst().get());
            status.setSuccess(true);
        }
        Gson g = new Gson();
        return g.toJson(status);
    }
}
