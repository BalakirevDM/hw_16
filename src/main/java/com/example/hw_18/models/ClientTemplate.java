package com.example.hw_18.models;

import java.util.Date;

public class ClientTemplate {
    public String name;
    public Integer age;
    public Integer amount;
    public boolean isCreditApproved;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public boolean isCreditApproved() {
        return isCreditApproved;
    }

    public void setCreditApproved(boolean creditApproved) {
        isCreditApproved = creditApproved;
    }

    public Client toClient() {
        Client temp = new Client();
        temp.setName(name);
        temp.setAge(age);
        temp.setCreditAmount(amount);
        temp.setApproved(false);
        temp.setId(new Date().getTime());
        return temp;
    }
}
