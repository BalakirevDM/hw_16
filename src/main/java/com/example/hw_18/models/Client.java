package com.example.hw_18.models;

import java.util.ArrayList;
import java.util.List;

/*@Data // автосоздание полей гет/сет, а также перепишет toString
@NoArgsConstructor // пустой конструктор
@AllArgsConstructor // конструктор со всеми полями
@Builder  // шаблон билдер*/
public class Client {
    private String name;
    private int age;
    private int amount;
    private long id;
    private boolean isApproved;

    public Client() {
    }

    public static List<Client> clients = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getCreditAmount() {
        return amount;
    }

    public void setCreditAmount(int creditAmount) {
        this.amount = creditAmount;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isApproved() {
        return isApproved;
    }

    public void setApproved(boolean approved) {
        isApproved = approved;
    }

/*    public void save(String path, ObjectMapper mapper) throws IOException {
        mapper.writeValue(new File(path), this);
    }*/
}
