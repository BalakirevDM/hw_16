package com.example.hw_16.endpoints;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


import static com.example.hw_16.logic.firstApiLogic.multiLogic;

@RestController
public class MyFirstApi {
    @RequestMapping (value = "/multi/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String multi(@PathVariable int id) {
        return multiLogic(id);
    }
}
